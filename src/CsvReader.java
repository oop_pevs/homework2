import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

class OneTask implements Comparable<OneTask>
{
    String date;
    String note;
    String priority;
    Logger logger = new Logger();


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     *
     * @param date
     * @param note
     * @param priority
     */
    OneTask(String date, String note, String priority)
    {
        this.logger.setLevel(this.logger.LOG_LEVEL_DEBUG);
        this.logger.debug("Data in constructor: DATE:"+date+" NOTE:"+note+" PRIORITY:"+priority);

        this.date = date;
        this.note = note;
        this.priority = priority;
    }

    @Override
    public int compareTo(OneTask compareTask) {
        this.logger.debug("Task_1: "+this.getNote()+" vs Task_2: "+compareTask.getNote());

        int priority = compareTask.getPriority().compareTo(this.getPriority());
        int date = this.getDate().compareTo(compareTask.getDate());

        // If priority is same between two task than compare dates
        // Else compare just priority
        if (priority == 0) {
            return date;
        } else {
            return priority;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        return this.getDate() + "," + this.getNote() + "," + this.getPriority();
    }
}

class CsvReader
{
    OneTask[] allTasks;
    int top;
    int len;
    Logger logger = new Logger();

    /**
     *
     * @param size
     */
    CsvReader(int size)
    {
        this.logger.setLevel(this.logger.LOG_LEVEL_DEBUG);

        this.logger.debug("Data in constructor: SIZE:"+size);

        this.allTasks = new OneTask[size];
        this.top = 0;
        this.len = this.allTasks.length;
    }

    /**
     *
     * @param fileName
     */
    public void LoadCsv(String fileName)
    {
        try (BufferedReader br = new BufferedReader(new FileReader("toDo/"+fileName))) {

            // Read file by line
            String line;
            while ((line = br.readLine()) != null) {
                // Split lines by , because it is CSV separator
                String[] parts = line.split(",");
                OneTask oneTask;

                if (parts.length < 3) {
                    oneTask = new OneTask(parts[0], parts[1], "");
                } else {
                    oneTask = new OneTask(parts[0], parts[1], parts[2]);
                }

                if(this.top < this.len){
                    this.allTasks[this.top] = oneTask;
                    this.top ++;
                } else {
                    // Logger
                    System.out.println("Stack is full");
                }
            }
        } catch (IOException e) {
            this.logger.error("Exception in LoadCsv:"+e.toString());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public OneTask[] getAllTasks() {
        this.logger.info("Start sorting array");

        Arrays.sort(this.allTasks, 0, this.top);

        return this.allTasks;
    }

}

class CsvWriter
{
    OneTask[] allTasks;
    int len;
    Logger logger = new Logger();

    CsvWriter(OneTask[] data, int len)
    {
        this.logger.setLevel(this.logger.LOG_LEVEL_DEBUG);

        this.allTasks = data;
        this.len = len;
    }

    public void FillCsv()
    {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("done/output.csv", true));
            this.logger.info("File is open");

            for (int i=0; i < this.len; i++) {
                writer.append(this.allTasks[i].toString() + ",done\n");
            }
            writer.close();
            this.logger.info("File is closed");

        } catch (IOException e) {
            this.logger.error("Exception in FillCsv:"+e.toString());
            e.printStackTrace();
        }
    }

}

class MainProgram
{
    public static void main(String args[])
    {

        CsvReader csvReader = new CsvReader(100);
        boolean exit = false;

        while (!exit) {
            int option = 0;

            // Show menu
            System.out.println("1) Load csv file");
            System.out.println("2) Run program");
            System.out.println("0) Exit");
            System.out.print("Select your option: ");

            // Try to load option from menu
            try {
                option = System.in.read();
            } catch (IOException e) {
                // Logger
                e.printStackTrace();
            }


            if (option == '1') {
                System.out.print("Enter CSV file name:");
                Scanner scanner = new Scanner(System.in);
                scanner.nextLine();
                String fileName = scanner.nextLine();

                csvReader.LoadCsv(fileName);
            }

            if (option == '2' && csvReader.top > 0) {
                OneTask[] dataTask = csvReader.getAllTasks();
                int tasksLength = csvReader.top;

                CsvWriter csvWriter = new CsvWriter(dataTask, tasksLength);
                csvWriter.FillCsv();
            } else if (option != '0') {
                System.out.println("\nFirst you need to load CSV file...");
            }


            // Stop program if option is 0
            if (option == '0') {
                exit = true;
            }

        }
    }
}